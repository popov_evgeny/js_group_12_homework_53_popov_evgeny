import {Component} from '@angular/core';

@Component({
  selector: 'app-todolist',
  templateUrl: './todolist.component.html',
  styleUrls: ['./todolist.component.css']
})
export class TodolistComponent {
  task = '';
  tasks = [
    {task: 'New task 1'},
    {task: 'New task 2'},
    {task: 'New task 3'}
  ];

  addTask(event: Event) {
    event.preventDefault();
    this.tasks.push({
      task: this.task
    });
    this.task = '';
  }

  onDeleteTask(index:number) {
    this.tasks.splice(index, 1);
  }

  onChangeTask(i: number, newTask: string) {
    this.tasks[i].task = newTask;
  }
}
