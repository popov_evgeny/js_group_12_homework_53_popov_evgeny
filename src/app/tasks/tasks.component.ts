import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.css']
})
export class TasksComponent {
  @Input() task = '';
  @Input() symbol = '';
  @Output() delete = new EventEmitter();
  @Output() changeTask = new EventEmitter<string>();
  classFocus = '';
  hover = '';

  onClickDelete() {
    this.delete.emit();
  }

  onTaskInput(event: Event) {
    const target = <HTMLInputElement>event.target;
    this.changeTask.emit(target.value);
  }

  onDone() {
    this.symbol = '✓';
  }

  getInputClass() {
    return {
      input: true,
      text: this.symbol
    }
  }

  onFocus() {
    this.classFocus = 'focus';
  }

  offFocus(){
    this.classFocus = '';
  }

  onHover() {
    this.hover = 'hover';
  }

  offHover() {
    this.hover = '';
  }
}
